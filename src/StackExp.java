public class StackExp extends Exception
{
	private static int num_except = 0;
	
	public StackExp(String msg)
	{
		super(msg);
		num_except++;
	}
	
	public static int GetNumExcept()
	{
		return num_except;
	}
}

