import java.util.ArrayList;
import java.util.HashMap;


public class Parser {
	public static String CYKtable;
	//TO Yogi : pindahin ke baca file. filenya pake rule yang udah ane refactor
	public static String Rule = 
			"PRO_ADV_INTR_PRE -> PRO_ADV_INTR PRE;"
			+ "PRO_NEG_ADV_INTR_PRE_NOM_KON -> PRO_NEG_ADV_INTR_PRE_NOM KON;"
			+ "S -> PRO_TR_GER_PRE_NOM_KON_INTR_PRE PRO;"
			+ "ADV -> bisa;"
			+ "GER -> berenang;"
			+ "PRO_TR_GER_PRE -> PRO_TR_GER PRE;"
			+ "PRO_NEG_ADV_INTR_PRE -> PRO_NEG_ADV_INTR PRE;"
			+ "PRO_ADV_INTR_PRE_NOM -> PRO_ADV_INTR_PRE NOM;"
			+ "GER_KOP -> GER KOP;"
			+ "GER_KOP_NOM -> GER_KOP NOM;"
			+ "NOM -> ubi;"
			+ "NOM_NOM -> NOM NOM;"
			+ "S -> NOM_NOM_ADV ADJ;"
			+ "PRE -> dengan;"
			+ "PRO -> saya;"
			+ "PRO_ADV_INTR_PRE_NOM_KON_PRO -> PRO_ADV_INTR_PRE_NOM_KON PRO;"
			+ "PRO_INTR_DET_NOM_PRE -> PRO_INTR_DET_NOM PRE;"
			+ "DET -> itu;"
			+ "S -> PRO_ADV_GER_PRE_TR NOM;"
			+ "TR -> melakukannya;"
			+ "PRO_ADV_INTR_PRE_NOM_KON_PRO_INTR_INTR_NOM -> PRO_ADV_INTR_PRE_NOM_KON_PRO_INTR_INTR NOM;"
			+ "S -> PRO_ADV_INTR_PRE_NOM_KON_PRO_INTR_INTR_NOM_PRE NOM;"
			+ "GER_NOM_PRE_GER_DET_ADV_ADJ_KON_PRO_ADV -> GER_NOM_PRE_GER_DET_ADV_ADJ_KON_PRO ADV;"
			+ "INTR -> berenang;"
			+ "PRO_NEG_ADV_INTR_PRE_NOM_KON_INTR -> PRO_NEG_ADV_INTR_PRE_NOM_KON INTR;"
			+ "PRO_KON_INTR_PRE_NOM_ADV -> PRO_KON_INTR_PRE_NOM ADV;"
			+ "PRO_TR_GER_PRE_NOM_KON_INTR_PRE -> PRO_TR_GER_PRE_NOM_KON_INTR PRE;"
			+ "ADJ -> aneh;"
			+ "TR -> makan;"
			+ "NOM -> apapun;"
			+ "NOM -> hobi;"
			+ "PRO_TR_GER_PRE_NOM_KON -> PRO_TR_GER_PRE_NOM KON;"
			+ "PRO_TR_GER_PRE_NOM -> PRO_TR_GER_PRE NOM;"
			+ "PRO_KON_INTR_PRE_NOM_ADV_INTR -> PRO_KON_INTR_PRE_NOM_ADV INTR;"
			+ "S -> GER_KOP_NOM PRO;"
			+ "S -> PRO_NEG_ADV_INTR_PRE_NOM_KON_INTR NOM;"
			+ "PRO_NEG_ADV_INTR -> PRO_NEG_ADV INTR;"
			+ "PRO_ADV -> PRO ADV;"
			+ "PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM_KON_PRO_NEG_INTR -> PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM_KON_PRO_NEG INTR;"
			+ "GER_NOM_PRE_GER_DET_ADV_ADJ -> GER_NOM_PRE_GER_DET_ADV ADJ;"
			+ "NOM -> sabuga;"
			+ "NEG -> tidak;"
			+ "PRO_ADV_INTR_PRE_NOM_KON_PRO_INTR -> PRO_ADV_INTR_PRE_NOM_KON_PRO INTR;"
			+ "NOM -> bisa;"
			+ "S -> PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM_KON_PRO_NEG_INTR NOM;"
			+ "PRO_KON_INTR_PRE -> PRO_KON_INTR PRE;"
			+ "PRO_NEG -> PRO NEG;"
			+ "PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM_KON -> PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM KON;"
			+ "INTR -> takut;"
			+ "KOP -> adalah;"
			+ "NOM -> sungai;"
			+ "S -> GER_NOM_PRE_GER_DET_ADV_ADJ_KON_PRO_ADV TR;"
			+ "PRO_ADV_INTR_PRE_NOM_KON_PRO_INTR_INTR_NOM_PRE -> PRO_ADV_INTR_PRE_NOM_KON_PRO_INTR_INTR_NOM PRE;"
			+ "GER_NOM_PRE_GER_DET_ADV -> GER_NOM_PRE_GER_DET ADV;"
			+ "PRO -> desanya;"
			+ "PRO_INTR_DET -> PRO_INTR DET;"
			+ "PRO_TR -> PRO TR;"
			+ "PRO_INTR -> PRO INTR;"
			+ "PRE -> sambil;"
			+ "PRO_NEG_ADV -> PRO_NEG ADV;"
			+ "NOM_NOM_ADV -> NOM_NOM ADV;"
			+ "PRO_ADV_GER -> PRO_ADV GER;"
			+ "TR -> suka;"
			+ "PRO_KON_INTR -> PRO_KON INTR;"
			+ "KON -> karena;"
			+ "INTR -> tinggal;"
			+ "PRO_ADV_INTR_PRE_NOM_KON_PRO_INTR_INTR -> PRO_ADV_INTR_PRE_NOM_KON_PRO_INTR INTR;"
			+ "PRO_TR_GER -> PRO_TR GER;"
			+ "ADJ -> mematikan;"
			+ "PRO_NEG_ADV_INTR_PRE_NOM -> PRO_NEG_ADV_INTR_PRE NOM;"
			+ "PRO -> dia;"
			+ "GER_NOM_PRE_GER_DET_ADV_ADJ_KON_PRO -> GER_NOM_PRE_GER_DET_ADV_ADJ_KON PRO;"
			+ "GER_NOM_PRE_GER_DET -> GER_NOM_PRE_GER DET;"
			+ "ADV -> memang;"
			+ "KON -> dan;"
			+ "PRO_KON_INTR_PRE_NOM -> PRO_KON_INTR_PRE NOM;"
			+ "PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE -> PRO_KON_INTR_PRE_NOM_ADV_INTR PRE;"
			+ "PRO_ADV_INTR -> PRO_ADV INTR;"
			+ "ADV -> sering;"
			+ "GER_NOM_PRE_GER -> GER_NOM_PRE GER;"
			+ "S -> PRO_INTR_DET_NOM_PRE NOM;"
			+ "PRO_KON -> PRO KON;"
			+ "S -> PRO_ADV INTR;"
			+ "GER_NOM_PRE -> GER_NOM PRE;"
			+ "PRE -> di;"
			+ "PRO -> pamanku;"
			+ "NOM -> desa;"
			+ "GER_NOM -> GER NOM;"
			+ "GER -> makan;"
			+ "PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM_KON_PRO -> PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM_KON PRO;"
			+ "ADV -> mau;"
			+ "PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM_KON_PRO_NEG -> PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM_KON_PRO NEG;"
			+ "NOM -> ular;"
			+ "KON -> yang;"
			+ "DET -> tiap;"
			+ "PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE_NOM -> PRO_KON_INTR_PRE_NOM_ADV_INTR_PRE NOM;"
			+ "PRO_INTR_DET_NOM -> PRO_INTR_DET NOM;"
			+ "PRO_TR_GER_PRE_NOM_KON_INTR -> PRO_TR_GER_PRE_NOM_KON INTR;"
			+ "PRO_ADV_GER_PRE_TR -> PRO_ADV_GER_PRE TR;"
			+ "GER_NOM_PRE_GER_DET_ADV_ADJ_KON -> GER_NOM_PRE_GER_DET_ADV_ADJ KON;"
			+ "INTR -> ada;"
			+ "NOM -> minggu;"
			+ "PRO_ADV_GER_PRE -> PRO_ADV_GER PRE;"
			+ "KON -> tapi;"
			+ "PRO_ADV_INTR_PRE_NOM_KON -> PRO_ADV_INTR_PRE_NOM KON;";
	
	// Grammar
	private ArrayList<String> StartSymbol;
	private HashMap NonTerminal; // store index for each production
	private ArrayList<String[]> Production; // store production A -> B C
	private ArrayList<String[]> UnitProduction; // store unit production A -> a
	
	public Parser() {
		StartSymbol = new ArrayList();
		NonTerminal = new HashMap();
		Production = new ArrayList();
		UnitProduction = new ArrayList();
		CYKtable = "";
	}
	
	//TO YOGI : ganti delimiter sesuai file baru
	private String[] parseCNF(String CNF) {
		String delimiter = "[;]+";
		return CNF.split(delimiter);
	}
	
	//TO YOGI : ganti delimiter sesuai file baru
	private String[] parseRule(String rule) {
		String delimiter = "[ ->]+";
		return rule.split(delimiter);
	}
	
	//TO YOGI : ganti delimiter sesuai file baru
	private String[] parseInput(String input) {
		String delimiter = "[ ,.]+";
		return input.split(delimiter);
	}
	
	//TO YOGI : ganti delimiter sesuai file baru
	public void buildGrammar(String CNF) {
		String[] rules = parseCNF(CNF);
		for (int i = 0; i < rules.length; i++) {
			String[] rule = parseRule(rules[i]);
			
			if (!NonTerminal.containsKey(rule[0])) {
				NonTerminal.put(rule[0], NonTerminal.size());
			}
			
			if (rule.length == 2) { // A -> a
				UnitProduction.add(rule);
			} else { // (rule.length == 3) { // A -> B C
				Production.add(rule);
			}
		}

		// Start symbol
		StartSymbol.add("S");
	}
	
	public boolean CYK(String input) {
		// Break down the input into list of words
		String[] words = parseInput(input.toLowerCase());
		
		// Initialize CYK table with false
		boolean[][][] table = new boolean[words.length][words.length][NonTerminal.size()];
		
		// 1 word long
		for (int i = 0; i < words.length; i++) { // start index of word from input (from 0)
			for (int j = 0; j < UnitProduction.size(); j++) {
				if (UnitProduction.get(j)[1].equals(words[i])) {
					table[i][0][(int) NonTerminal.get(UnitProduction.get(j)[0])] = true;
				}
			}
		}
		
		// 2 or more words
		for (int i = 1; i < words.length; i++) { // length of words
			for (int j = 0; j < words.length - i; j++) { // start index of word from input
				for (int k = 1; k <= i; k++) { // partition ratio of words of length i
					for (int l = 0; l < Production.size(); l++) {
						if (table[j][k - 1][(int) NonTerminal.get(Production.get(l)[1])] 
								&& table[j + k][i - k][(int) NonTerminal.get(Production.get(l)[2])]) {
							table[j][i][(int) NonTerminal.get(Production.get(l)[0])] = true;
						}
					}
				}
			}
		}
		
		// print CYK table
		String[] rules = new String[NonTerminal.size()];
		for (int i = 0; i < UnitProduction.size(); i++) {
			rules[(int) NonTerminal.get(UnitProduction.get(i)[0])] = UnitProduction.get(i)[0];
		}
		for (int i = 0; i < Production.size(); i++) {
			rules[(int) NonTerminal.get(Production.get(i)[0])] = Production.get(i)[0];
		}
		for (int i = 0; i < words.length; i++) {
			System.out.print("  " + words[i] + " \t ");
			CYKtable += "  " + words[i] + " \t ";
		}
		System.out.println();
		CYKtable += "\n";
		for (int i = 0; i < words.length; i++) { // length of words
			for (int j = 0; j < words.length - i; j++) { // start index of word from input
				System.out.print("| ");
				CYKtable += "| ";
				for (int k = 0; k < rules.length; k++) {
					if (table[j][i][k]) {
						System.out.print(rules[k] + ",");
						CYKtable += rules[k] + ",";
					}
				}
				System.out.print(" \t|");
				CYKtable += " \t|";
			}
			System.out.println();
			CYKtable += "\n";
		}
		System.out.println();
		CYKtable += "\n";
		
		for (int i = 0; i < StartSymbol.size(); i++) { // iterate over start symbol
			if (table[0][words.length - 1][(int) NonTerminal.get(StartSymbol.get(i))]) {
				return true;
			}
		}

		return false;
	}
	
}

