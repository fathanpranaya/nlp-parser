import java.io.*;
import java.lang.*;
import java.util.*;

public class Kode
{
	private String[][] line = new String[500][500];
	private String[] baris = new String[500];
	private int JmlToken;
	private int JmlLine;
	//line[x][y] dimana x adalah baris, y adalah kolom dimulai dari 1

	public Kode()
	{
		//Counter untuk token
		JmlToken = 0;
		JmlLine = 0;
	}

	public void ReadText(String T)
	{
		int i;
		String temp;

		for (i = 0; i < 500;i++)
		{ //Inisialisasi
			baris[i] = null;
		}

		StringTokenizer st = new StringTokenizer(T, "\n");

		i = 1;
		try
		{
			while (true)
			{
				temp = st.nextToken();
				baris[i] = temp;
				System.out.println("Baris ke-"+i+" : "+baris[i]);
				i++;
			}
		}
		catch (Exception e)
		{}

		JmlLine = i-1;
	}

	public void TextToLine() throws StackExp
	{
		for (int i = 0; i<500;i++)
		{
			for (int j = 0; j<500; j++)
			{
				line[i][j] = null;
			}
		}

		JmlToken = 0;
		int Sanitize = 0; //jumlah kurawal buka yang aktif
		int x = 1;
		int y = 1;
		String temp;
		boolean NextWhile = true;
		boolean Nextif = true;

		while (x <= JmlLine) //baca baris per line teks
		{
			StringTokenizer st = new StringTokenizer(baris[x], " "); //tokenizer (pembatas), saya definisikan :
			try //try untuk nextToken
			{
				y = 1;
				while (true) //ini agar terus2an ambil semua string dalam satu baris hingga exception
				{
					temp = st.nextToken();	
					if (temp.charAt(0) != '\0')
					{
						if (Sanitize <= 0)
						{
							if (temp.compareTo("{") == 0) Sanitize++;
							else if ((temp.compareTo("}") == 0) && (Sanitize < 0)) Sanitize--;
							else
							{
								/* Error Detection */
								line[x][y] = temp; //Next Token sebuah string pada sebuah line
								if (temp.compareTo("while") == 0)
									NextWhile = false;
								else if (temp.compareTo("do") == 0)
									NextWhile = true;
								else if (temp.compareTo("if") == 0)
									Nextif = false;
								else if (temp.compareTo("then") == 0)
									Nextif = true;
								/* End of Error Detection */	
								System.out.println("("+x+","+y+")"+" : "+line[x][y]);
								y++; //increment kolom
								JmlToken++; //Increment jumlah token
							}
						}
						else //jika sanitasi mode on
						{
							if (temp.compareTo("}") == 0) Sanitize--;
							else if (temp.compareTo("{") == 0) Sanitize++;
						}
					}
				}

			}
			catch (Exception e) //fungsi tangkap, jika nextToken = NULL
			{
				if (!NextWhile)
					throw new StackExp("STOP memparsing kode, fungsi while tanpa 'do' pada line "+x);

				if (!Nextif)
					throw new StackExp("STOP memparsing kode, fungsi if tanpa 'then' pada line "+x);
			}	
			x++; //increment baris
		}
	}	

	public void ReadFile(String Lokasi) throws StackExp
	{
		for (int i = 0; i<500;i++)
		{
			for (int j = 0; j<500; j++)
			{
				line[i][j] = null;
			}
		}

		JmlToken = 0;
		int Sanitize = 0; //jumlah kurawal buka yang aktif
		int x = 1;
		int y = 1;
		String temp;
		String baris;
		boolean NextWhile = true;
		boolean Nextif = true;

		//main.removeText();

		try
		{
			BufferedReader br = new BufferedReader(new FileReader(Lokasi));
			while ((baris = br.readLine()) != null) //baca baris per line teks
			{
				//main.addText(baris);
				StringTokenizer st = new StringTokenizer(baris, " "); //tokenizer (pembatas), saya definisikan :
				try //try untuk nextToken
				{
					y = 1;
					while (true) //ini agar terus2an ambil semua string dalam satu baris hingga exception
					{
						temp = st.nextToken();	

						if (Sanitize <= 0)
						{
							if (temp.compareTo("{") == 0) Sanitize++;
							else if ((temp.compareTo("}") == 0) && (Sanitize < 0)) Sanitize--;
							else
							{
								/* Error Detection */
								line[x][y] = temp; //Next Token sebuah string pada sebuah line
								if (temp.compareTo("while") == 0)
									NextWhile = false;
								else if (temp.compareTo("do") == 0)
									NextWhile = true;
								else if (temp.compareTo("if") == 0)
									Nextif = false;
								else if (temp.compareTo("then") == 0)
									Nextif = true;
								/* End of Error Detection */	
								//System.out.println("("+x+","+y+")"+" : "+line[x][y]);
								y++; //increment kolom
								JmlToken++; //Increment jumlah token
							}
						}
						else //jika sanitasi mode on
						{
							if (temp.compareTo("}") == 0) Sanitize--;
							else if (temp.compareTo("{") == 0) Sanitize++;
						}
					}

				}
				catch (Exception e) //fungsi tangkap, jika nextToken = NULL
				{
					if (!NextWhile)
						throw new StackExp("STOP memparsing kode, fungsi while tanpa 'do' pada line "+x);

					if (!Nextif)
						throw new StackExp("STOP memparsing kode, fungsi if tanpa 'then' pada line "+x);
				}	
				x++; //increment baris
			}
		} catch (Exception e) {
			System.out.println("Error : "+e.getMessage());
		}
	}

	public String GetStr(int X, int Y)
	{
		//Getter untuk line
		return line[X][Y];
	}

	public int GetJmlToken()
	{
		//Getter untuk jumlah token yang telah dibaca
		return JmlToken;
	}

	public int JmlStr(String Input)
	{
		/* Mencari Jumlah string input pada kode */
		int Jml=0;
		int X = 1;
		int Y = 1;
		int JmlString = 0;
		while (JmlString < JmlToken) //sampai ketemu null
		{
			while (line[X][Y] != null) //sampai ketemu null
			{
				//System.out.println("DEBUG : "+line[X][Y]);
				if (line[X][Y].compareTo(Input) == 0)
				{
					Jml++;	
				}
				Y++;
				JmlString++;
			}
			X++;
			Y = 1;
		}
		return Jml;
	}

	public void StatusKode() throws StackExp
	{
		//Menghitung status kevalidan sebuah kode berdasarkan kecocokan pasangan
		/* Mengembalikan nilai 0 apabila OK, semua jumlah sama / cocok
	       Mengembalikan nilai > 0 apabila invalid, ada yang tidak cocok jumlahnya
		 */
		if (
				(JmlStr("begin") == JmlStr("end")) && 
				(JmlStr("(") == JmlStr(")")) && 
				(JmlStr("repeat") == JmlStr("until")) &&		  
				(JmlStr("while") == JmlStr("do"))			
				)
		{}
		else if (JmlStr("(") > JmlStr(")"))
		{
			throw new StackExp("Sintaks Error : Tanda kurung buka lebih banyak daripada tanda kurung tutup");
		}
		else if (JmlStr("(") < JmlStr(")"))
		{
			throw new StackExp("Sintaks Error : Tanda kurung buka lebih sedikit daripada tanda kurung tutup");
		}
		else if (JmlStr("begin") > JmlStr("end"))
		{
			throw new StackExp("Sintaks Error : Tidak ada end (begin Kelebihan)");
		}
		else if (JmlStr("begin") < JmlStr("end"))
		{
			throw new StackExp("Sintaks Error : Tidak Ada begin (end Kelebihan)");
		}
		else if (JmlStr("repeat") > JmlStr("until"))
		{
			throw new StackExp("Sintaks Error : Kurang tanda Until");
		}
		else if (JmlStr("repeat") < JmlStr("until"))
		{
			throw new StackExp("Sintaks Error : until kebanyakan");
		}
		else if (JmlStr("while") > JmlStr("do"))
		{
			throw new StackExp("Sintaks Error : Kurang tanda 'do'");
		}
		else if (JmlStr("while") < JmlStr("do"))
		{
			throw new StackExp("Sintaks Error : kelebihan 'do'");
		}
	}
}

/* End of File Kode.java */