import java.io.*;
import java.lang.*;
import java.util.*;

public class Rule
{
	private String[][] line = new String[200][200];
	private int JmlRule;
	//line[x][y] dimana x adalah baris, y adalah kolom dimulai dari 1
	
	public Rule()
	{}
	
	public void ReadFile(String Lokasi)
	{
		JmlRule = 0;
		int x = 1;
		int y = 1;
		String baris;
		String temp;
		JmlRule = 0;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(Lokasi));
			while ((baris = br.readLine()) != null) //baca baris per line teks
			{
				StringTokenizer st = new StringTokenizer(baris, " "); //tokenizer (pembatas), saya definisikan :
				
				try //try untuk nextToken
				{
					//Rumus : <Kolom 1> -> <Kolom 2> <Kolom 3>
					y = 1; //Kolom 1
					line[x][y] = st.nextToken();
					
					temp = st.nextToken(); //buang tanda "->"
					
					y = 2; //Kolom 2
					line[x][y] = st.nextToken();
					
					y = 3; //Kolom 3
					line[x][y] = st.nextToken();
					x++;
					JmlRule++;
				}
				catch (Exception e) //fungsi tangkap, jika nextToken = NULL
				{}
				
			}
		} catch (Exception e) {
            System.out.println("Error : "+e.getMessage());
        }
	}
	
	public String GetStr(int X, int Y)
	{
		//Getter untuk line
		return line[X][Y];
	}
	
	public String GetRule(String S1, String S2) //P dan NP masih mengembalikan NULL
	{
		//Rumus : <output> -> <S1> <S2>
		//Output adalah rule untuk S1 dan S2
		
		int x = 1;
		int JmlString = 1;
		while (JmlString <= JmlRule) //kalau sampai ketemu kosong
		{
			if (line[x][3] == null)
			{
				if (line[x][2].compareTo(S1) == 0)
				{
					return line[x][1];
				}
			}
			else
			{
				if ((line[x][2].compareTo(S1) == 0) && (line[x][3].compareTo(S2) == 0))
				{
					return line[x][1];
				}
			}
			x++;
			JmlString++;
		}
		return "NULL"; //Jika tidak ketemu, kembaliannya string "NULL"
	}
	
	public String GetDefine(String input)
	{
		//Rumus : <output> -> <input>
		//Dimana output adalah define dari input
		int x = 1;
		int JmlString = 1;
		while (JmlString <= JmlRule)
		{
			if (line[x][2].equals(input))
			{
				return line[x][1];
			}
			x++;
			JmlString++;
		}
		return "NULL"; //Jika tidak ketemu, kembaliannya string "NULL"
	}
}

