operator -> +
operator -> -
operator -> *
operator -> /
operand -> a
operand -> b
operand -> c
operand -> d
operand -> e
operand -> f
operand -> g
operand -> h
operand -> i
operand -> j
operand -> k
operand -> l
operand -> m
operand -> n
operand -> o
operand -> p
operand -> q
operand -> r
operand -> s
operand -> t
operand -> u
operand -> v
operand -> w
operand -> x
operand -> y
operand -> z
operand -> 0
operand -> 1
operand -> 2
operand -> 3
operand -> 4
operand -> 5
operand -> 6
operand -> 7
operand -> 8
operand -> 9

as_assignment -> =

if_jika -> if
if_setelah -> then
if_selain -> else

io_io -> input
io_io -> output
io_isi -> apapun
kurungbuka -> (
kurungtutup -> )

mulai -> begin
selesai -> end

ki_operator -> <
ki_operator -> >
ki_operator -> <=
ki_operator -> >=
ki_operator -> ==
ki_operator -> !=

rt_ulang -> repeat
rt_sampai -> until

wl_ketika -> while
wl_jalankan -> do
